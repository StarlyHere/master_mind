import random

# List of possible words
#word_list = [] we should add words from file

secret_word = random.choice(word_list)

guesses = 6
guessed_word = ["_"] * len(secret_word)

while guesses > 0:
    guess = input("Enter your guess: ").lower()

    if guess == secret_word:
        print("You Won")
        break
    else:
        feedback = 0
        for i in range(len(secret_word)):
            if guess[i] == secret_word[i]:
                feedback += 1
            elif guess[i] in secret_word:
                feedback += 1
            else:
                continue
        print("Feedback:", " ".join(feedback))
        guesses -= 1
        print(f"Attempts left: {guesses}")

if guesses == 0:
    print(f"Out of attempts! The secret word was '{secret_word}'.")